import java.io.*;
import java.util.*;

public class ReadingFromTrajFiles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				String filePath = "F:\\Geolife Trajectories 1.3\\Data\\000\\Trajectory";
				File datasetFolder = new File(filePath);
			    File[] files = datasetFolder.listFiles();
			    
			    try
			    {
			    
			    Formatter output1 = new Formatter("All_insert_queries_of_000.sql");
			    String tableMake = "DROP TABLE IF EXISTS trajectory_table_without_indexing CASCADE;\nCREATE TABLE trajectory_table_without_indexing(id BIGINT, tr trajectory);";
			    output1.format("%s", tableMake);
			    Formatter output2 = new Formatter("For_indexing_All_insert_queries_of_000.sql");
			    tableMake = "DROP TABLE IF EXISTS trajectory_table CASCADE;\nCREATE TABLE trajectory_table of trajectory;\nCREATE INDEX temporal_index ON trajectory_table USING btree ( tsrange(s_time, e_time));\nCREATE INDEX spatial_index ON trajectory_table USING GIST (bbox);\nVACUUM trajectory_table;";
			    output2.format("%s", tableMake);

			    for(int i=0;i<files.length;i++)
			    {
			    	//String fileName = files[i].toString();
			    	//System.out.println(fileName);
			    	System.out.println("Reading file "+i);
			    	BufferedReader b1 = new BufferedReader(new FileReader(files[i]));
			    	//Formatter output = new Formatter("INSERT_QUERIES_"+i+".txt");
			    	//System.out.println("Alive");
			    	String insertQuery = "INSERT INTO trajectory_table_without_indexing(id, tr) VALUES ("+i+",\n _trajectory( ARRAY [\n";
			    	String insertForIndexing = "SELECT _trajectory_2(\nARRAY[\n";
			    	int lineCount = 0;
			    	String line;
			    	while((line = b1.readLine())!=null)
			    	{
			    		lineCount++;
			    		if(lineCount<=6)
			    			continue;
			    		String[] vals = line.split(",");
			    		double x_coord = Double.parseDouble(vals[0]);
			    		double y_coord = Double.parseDouble(vals[1]);
			    		String date = vals[5];
			    		String time = vals[6];
			    		insertQuery = insertQuery + "ROW (";
			    		insertForIndexing = insertForIndexing + "ROW (";
			    		insertQuery = insertQuery + " TIMESTAMP '"+date+" "+time+"', ";
			    		insertForIndexing = insertForIndexing + " TIMESTAMP '"+date+" "+time+"', ";
			    		insertQuery = insertQuery + "ST_GeomFromText('"+"POINT("+x_coord+" "+y_coord+")"+"')";
			    		insertForIndexing = insertForIndexing + "ST_GeomFromText('"+"POINT("+x_coord+" "+y_coord+")"+"')";
			    		insertQuery = insertQuery + ")::tg_pair, \n";
			    		insertForIndexing = insertForIndexing + ")::tg_pair, \n";
			    	}
			    	insertQuery = insertQuery.substring(0, insertQuery.length()-3);
			    	insertForIndexing = insertForIndexing.substring(0,insertForIndexing.length()-3);
			    	insertQuery = insertQuery + "\n]::tg_pair[]));";
			    	insertForIndexing = insertForIndexing + "]::tg_pair[],\n'trajectory_table'::REGCLASS\n);";
			    	output1.format("--Traj entry --> %d\n", i);
			    	output1.format("%s\n\n\n", insertQuery);
			    	output2.format("--Traj entry --> %d\n", i);
			    	output2.format("%s\n\n\n", insertForIndexing);
			    	//output.format("%s\n", insertQuery);
			    	//output.close();
			    	b1.close();
			    	
			    	//if(i==2)
			    		//break;
			    } //for
			    output1.close();
			    output2.close();
			    } //try
			    catch(Exception e)
		    	{
		    		System.out.println("Error in reading from file");
		    		System.exit(0);
		    	}
			    System.out.println("Finished !!!");

	}

}
